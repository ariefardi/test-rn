import React from 'react';
import {Text} from 'react-native';

const NotificationEdit = ({props}) => {
  if (props.data.length === 8) {
    return (
      <Text style={{fontSize: 15, fontWeight: '700', margin: 8}}>
        Remove one of the services before adding another one one
      </Text>
    );
  }
  return (
    <Text style={{fontSize: 15, fontWeight: '700', margin: 8}}>
      Fill in the tiles with your top 7 favorite services. Just tap 'em one by
      one
    </Text>
  );
};

export default NotificationEdit;
