import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import MyIcon from './Icon';

const ListCategory = props => {
  return (
    <FlatList
      data={props.data}
      renderItem={({item}) => (
        <TouchableOpacity
          onPress={() => {
            props.iconAction(item.id, true, item);
          }}>
          <View style={styles.categoryContainer}>
            {props.editMode && item.id !== 'h' && (
              <View style={styles.badges}>
                <MyIcon icon={'trash'} size={10} color={'grey'} />
              </View>
            )}
            <View
              style={{...styles.iconContainer, backgroundColor: item.color}}>
              <MyIcon icon={item.icon} />
            </View>
            <View>
              <Text>{item.value}</Text>
            </View>
          </View>
        </TouchableOpacity>
      )}
      keyExtractor={item => item.id}
      numColumns={numColumns}
    />
  );
}
const numColumns = 4;
const size = Dimensions.get('window').width / numColumns;
const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  categoryContainer: {
    flex: 1,
    width: size,
    height: size,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconContainer: {
    backgroundColor: '#900',
    padding: 8,
    borderRadius: 50,
  },
  badges: {
    position: 'absolute',
    left: 25,
    top: 15,
  },
});
export default ListCategory;
