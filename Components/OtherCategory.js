import React from 'react';
import {
  Dimensions,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import MyIcon from './Icon';
const OtherCategory = ({props}) => (
  <View style={styles.container}>
    <View style={{flexGrow: 1}}>
      <Text style={styles.otherCategory}>Other Category</Text>
    </View>
    <FlatList
      data={props.others}
      renderItem={({item}) => (
        <TouchableOpacity
          onPress={() => {
            props.data.length < 8 &&
              !props.checkStatusColor(item.id) &&
              props.addCategory(item);
          }}>
          <View style={styles.categoryContainer}>
            {props.editMode && props.data.length < 8 && (
              <View style={styles.badges}>
                <MyIcon
                  icon={'plus'}
                  size={10}
                  color={props.checkStatusColor(item.id) ? 'grey' : 'red'}
                />
              </View>
            )}
            <View
              style={{...styles.iconContainer, backgroundColor: item.color}}>
              <MyIcon icon={item.icon} />
            </View>
            <View>
              <Text>{item.value}</Text>
            </View>
          </View>
        </TouchableOpacity>
      )}
      keyExtractor={item => item.id}
      numColumns={numColumns}
    />
  </View>
);
const numColumns = 4;
const size = Dimensions.get('window').width / numColumns;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-end',
    marginVertical: 4,
    marginHorizontal: 8,
  },
  categoryContainer: {
    flex: 1,
    width: size,
    height: size,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconContainer: {
    backgroundColor: '#900',
    padding: 8,
    borderRadius: 50,
  },
  badges: {
    position: 'absolute',
    left: 25,
    top: 15,
  },
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  otherCategory: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});

export default OtherCategory;
