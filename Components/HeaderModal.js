import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';

import MyIcon from './Icon';
const HeaderModal = ({props}) => (
  <React.Fragment>
    <View
      style={{
        flex: 1,
        alignItems: 'center',
        marginVertical: 4,
        marginHorizontal: 8,
      }}>
      <TouchableOpacity
        onPress={() => {
          props.closeModal();
        }}>
        <MyIcon icon={'ellipsis-h'} size={40} color={'faded'} />
      </TouchableOpacity>
    </View>
    <View
      style={{
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-end',
        marginVertical: 4,
        marginHorizontal: 8,
      }}>
      <View style={{flexGrow: 1}}>
        <Text style={styles.yourFavorite}>Your Favorites</Text>
      </View>
      <View style={{marginTop: 4}}>
        {props.editMode ? (
          props.data.length === 8 ?
            <TouchableOpacity
              style={styles.buttonNormal}
              onPress={() => {
                props.saveIconAction();
              }}>
              <Text style={{color: Colors.white, fontWeight: 'bold'}}>
                Save
              </Text>
            </TouchableOpacity> :
            <View style={styles.buttonOutlineDisabled}>
              <Text style={{color: Colors.dark, fontWeight: 'bold'}}>
                Save
              </Text>
            </View>
        ) : (
          <TouchableOpacity
            style={styles.buttonOutline}
            onPress={() => {
              props.setEditMode(!props.editMode);
            }}>
            <Text style={{color: '#4CAF50', fontWeight: 'bold'}}>Edit</Text>
          </TouchableOpacity>
        )}
      </View>
    </View>
  </React.Fragment>
);
export default HeaderModal;

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  yourFavorite: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  buttonOutline: {
    width: '100%',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: '#4CAF50',
    backgroundColor: Colors.white,
    paddingHorizontal: 8,
    paddingVertical: 4,
    borderRadius: 5,
  },
  buttonOutlineDisabled: {
    width: '100%',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: 'grey',
    backgroundColor: Colors.white,
    paddingHorizontal: 8,
    paddingVertical: 4,
    borderRadius: 5,
  },
  buttonNormal: {
    width: '100%',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: Colors.primary,
    backgroundColor: Colors.primary,
    paddingHorizontal: 8,
    paddingVertical: 4,
    borderRadius: 5,
  },
});
