import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';

const MyIcon = ({icon = 'ellipsis-h', size = 30, color = 'white'}) => (
  <Icon name={icon} size={size} color={color} />
);
export default MyIcon;
