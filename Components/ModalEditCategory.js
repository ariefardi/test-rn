import React from 'react';
import {ScrollView, Modal, Text} from 'react-native';

import HeaderModal from './HeaderModal';
import ListCategory from './ListCategory';
import OtherCategory from './OtherCategory';
import NotificationEdit from './NotificationEdit';

const ModalEditCategory = props => {
  return (
    <Modal
      animationType="slide"
      transparent={false}
      visible={props.modalVisible}>
      <ScrollView>
        <HeaderModal props={{...props}} />
        {props.editMode && <NotificationEdit props={{...props}} />}
        <ListCategory
          editMode={props.editMode}
          data={props.data}
          modalVisible={props.modalVisible}
          iconAction={props.iconAction}
        />
        <OtherCategory props={{...props}} />
      </ScrollView>
    </Modal>
  );
};

export default ModalEditCategory;
