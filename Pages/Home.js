import React, {Component} from 'react';
import {StyleSheet, ScrollView} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';

import ListCategory from '../Components/ListCategory';
import ModalEditCategory from '../Components/ModalEditCategory';
export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      data: [
        {id: 'a', value: 'Ride', icon: 'car', color: '#900'},
        {id: 'b', value: 'Car', icon: 'car', color: '#900'},
        {id: 'c', value: 'Good', icon: 'car', color: '#900'},
        {id: 'd', value: 'Taxi', icon: 'car', color: '#900'},
        {id: 'e', value: 'Send', icon: 'car', color: '#900'},
        {id: 'f', value: 'Pulsa', icon: 'car', color: '#900'},
        {id: 'g', value: 'Clean', icon: 'car', color: '#900'},
        {id: 'h', value: 'More', icon: 'th-large', color: '#900'},
      ],
      backup: [],
      others: [
        {id: 'a', value: 'Ride', icon: 'car', color: '#900'},
        {id: 'b', value: 'Car', icon: 'car', color: '#900'},
        {id: 'c', value: 'Food', icon: 'car', color: '#900'},
        {id: 'd', value: 'Taxi', icon: 'car', color: '#900'},
        {id: 'e', value: 'Send', icon: 'car', color: '#900'},
        {id: 'f', value: 'Pulsa', icon: 'car', color: '#900'},
        {id: 'g', value: 'Clean', icon: 'car', color: '#900'},
        {id: 'i', value: 'Massage', icon: 'taxi', color: 'orange'},
        {id: 'j', value: 'Glam', icon: 'taxi', color: 'orange'},
        {id: 'k', value: 'Fitness', icon: 'taxi', color: 'orange'},
        {id: 'l', value: 'Auto', icon: 'taxi', color: 'orange'},
        {id: 'l', value: 'Shop', icon: 'taxi', color: 'orange'},
      ],
      editMode: false,
    };
    this.iconAction = this.iconAction.bind(this);
    this.setEditMode = this.setEditMode.bind(this);
    this.saveIconAction = this.saveIconAction.bind(this);
    this.checkStatusColor = this.checkStatusColor.bind(this);
    this.addCategory = this.addCategory.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }
  closeModal() {
    this.setState({
      modalVisible: false,
      editMode: false,
      data: this.state.backup,
    });
  }
  iconAction(id, value) {
    if (id === 'h' && !this.state.editMode) {
      this.setState({
        modalVisible: value,
        editMode: false,
        backup: this.state.data,
      });
    }
    if (id !== 'h' && this.state.editMode) {
      let data = this.state.data;
      data = data.filter(d => d.id !== id);
      this.setState({
        data,
      });
    }
  }
  saveIconAction() {
    this.setState({modalVisible: false, editMode: false});
  }
  addCategory(item) {
    let data = this.state.data;
    data.splice(data.length - 1, 0, item);
    this.setState({
      data,
    });
  }
  setEditMode(value) {
    this.setState({editMode: value});
  }
  checkStatusColor(id) {
    let data = this.state.data;
    let find = data.find(x => x.id === id);
    return find;
  }
  render() {
    const {data, editMode, modalVisible, others} = this.state
    return (
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={styles.scrollView}>
        <ListCategory
          data={data}
          iconAction={this.iconAction}
          modalVisible={modalVisible}
        />
        <ModalEditCategory
          iconAction={this.iconAction}
          data={data}
          setEditMode={this.setEditMode}
          editMode={editMode}
          modalVisible={modalVisible}
          saveIconAction={this.saveIconAction}
          others={others}
          checkStatusColor={this.checkStatusColor}
          addCategory={this.addCategory}
          closeModal={this.closeModal}
        />
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
});
